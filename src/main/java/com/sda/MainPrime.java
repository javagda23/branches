package com.sda;

import java.util.Scanner;

public class MainPrime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        while (--n >= 0) {
            int liczba = scanner.nextInt();

            System.out.println(sprawdz(liczba) ? "TAK" : "NIE");
        }
    }

    private static boolean sprawdz(int liczba) {
        if (liczba == 1) return false;

        double sqrtLiczba = Math.sqrt(liczba);
        for (int i = 2; i <= sqrtLiczba; i++) {
            if (liczba % i == 0) {
                return false;
            }
        }
        return true;
    }
}
